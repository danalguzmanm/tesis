<?php

$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
}
?>
<html>

<head>
	<title>F</title>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<link rel="stylesheet" href="css/estilos.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.js"></script>
</head>

<body>
	<?php
	include "presentacion/encabezado.php";
	include "presentacion/menu.php";
	if($pid==""){
		include "presentacion/inicio.php";
	}else{
		include $pid;
	}
	
	?>
</body>

</html>