<?php
$seccion = "";
if (isset($_GET["sec"])) {
    $seccion = "#" . $_GET["sec"];
}
$var = count(glob('pagina/inicio/{*.php}', GLOB_BRACE));
?>
<div class="iconos">
    <div class="row ">
        <div class="col-lg-1 x">
            <div>
                <button class="btn f" type="submit" data-toggle="collapse" data-target="#tipo" aria-expanded="false" aria-controls="multiCollapseExample2">
                    <h6><i class="fas fa-palette"></i></h6>
                </button>
            </div>
            <div>
                <br>
            </div>
            <div>
                <button class="btn f" type="submit" data-toggle="collapse" data-target="#secc" aria-expanded="false" aria-controls="multiCollapseExample" onclick="seccion()">
                    <h6><i class="fas fa-plus-circle"></i></h6>
                </button>
            </div>
            <div>
                <br>
            </div>
            <div>
                <button class="btn f" type="submit" data-toggle="collapse" data-target="" aria-expanded="false" aria-controls="multiCollapseExample">
                    <h6><i class="fas fa-trash-alt"></i></h6>
                </button>
            </div>
            <div>
                <br>
            </div>
            <div>
                <a class="f" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <!-- <img src="" style="width: 7vh;"> -->
                </a>
            </div>
        </div>
        <div class="col-lg-11 x2">
            <div class="collapse" id="secc">
                
            </div>
            <div class="collapse" id="tipo">

            </div>
        </div>
    </div>
</div>
<input id="secciones" type="hidden" value="0" class="form-control" min="0" max="<?php echo $var - 1; ?>" oninput="cambiar(this.value)">
<div class="container-lg container-xl mt-5 mb-5">

    <div class="card shadow-lg vista-pagina">
        <div class="card-body" style="margin-top: 5vh;">
            <div>
                <h5>TU PAGINA EN TIEMPO REAL</h5>
            </div>
            <div>
                <div id="vista">

                </div>

            </div>
        </div>
        <div class="tarjeta-footer">
            <div class="row">
                <div class="col-12 col-lg-10 col-xl-10 d-felx d-flex justify-content-center align-self-center">
                    <div id="paginacion">
                        
                    </div>
                    <br>
                </div>

                <div class="col-12 col-lg-2 col-xl-2 d-flex justify-content-center align-self-center ">
                    <a href="pagina/index.php" role="button" id="Terminar" type="button" value=0 class="btn btn-dark float-right">Terminar</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function cam(x) {
        if (x == 0) {
            var val = $("#secciones").val();
            $("#secciones").val(parseInt(val, 10) + 1);
            cambiar(parseInt(val, 10) + 1);
        } else {
            var val = $("#secciones").val();
            $("#secciones").val(parseInt(val, 10) - 1);
            cambiar(parseInt(val, 10) - 1);
        }
    }

    function cambiar(val) {
        if (val == 0) {
            $("#SeccionNombre").val("Inicio");
        } else {
            $("#SeccionNombre").val("Seccion " + val);
        }
        $("#secciones").val(val);
        $.ajax({
            url: "presentacion/edicion/editarArchivo.php?v=14&sec=" + val,
            type: "GET",
            processData: false,
            contentType: false,
            cache: false,
        })
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/header.php") ?>&b=0&sec=" + val;
            $("#vista").load(url);
        })
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/paginacion.php") ?>&b=" + val;
            $("#paginacion").load(url);
        })
    }

    function nombre(val) {
        $.ajax({
            url: "presentacion/edicion/editarArchivo.php?v=13&nom=" + val + "&sec=" + $("#secciones").val(),
            type: "GET",
            processData: false,
            contentType: false,
            cache: false,
        })
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/header.php") ?>";
            $("#vista").load(url);
        })
    }

    var cont = 0; // identificador de pagina

    if (cont == 0) {
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/header.php") ?>";
            $("#vista").load(url);
        })
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/TipoAjax.php") ?>&t=0";
            $("#tipo").load(url);
        })
    }

    function seccion() {
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/seccion.php") ?>";
            $("#content").load(url);
        })
    }
</script>