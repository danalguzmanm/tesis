<?php
$edicion = 0;
$car = "";
if (isset($_GET["x"])) {
    $car = "#seccion" . $_GET["x"];
}
/* Condicional cambio de colores */
if (isset($_GET["val"])) {
    $color = $_GET["val"];
    Editar("--" . $_GET["ed"] . ": ", "pagina/css/estilos.css", "\t--" . $_GET["ed"] . ": #" . $color . ";");
}
/* Condicional añadir secciones galeria*/
if (isset($_GET["a"])) {
    $var = count(glob('pagina/inicio/{*.php}', GLOB_BRACE));
    copy("plantillas/Galeria/galeria" . $_GET["a"] . ".php", "pagina/inicio/seccion" . $var . ".php");
    eliminar("<!-- ed -->", "pagina/inicio/inicio.php", "");
    Editar("<!-- area" . $var . " -->", "pagina/inicio/inicio.php", "\t<!-- area" . $var . " -->" . PHP_EOL . "\t<!-- ed --><div class='secos'><br><h6>EDITANDO ESTA SECCION</h6></div>");
}
/* Condicional añadir secciones texto*/
if (isset($_GET["text"])) {
    $var = count(glob('pagina/inicio/{*.php}', GLOB_BRACE));
    copy("plantillas/Texto/texto" . $_GET["text"] . ".php", "pagina/inicio/seccion" . $var . ".php");
    eliminar("<!-- ed -->", "pagina/inicio/inicio.php", "");
    Editar("<!-- area" . $var . " -->", "pagina/inicio/inicio.php", "\t<!-- area" . $var . " -->" . PHP_EOL . "\t<!-- ed --><div class='secos'><br><h6>EDITANDO ESTA SECCION</h6></div>");
}
/* Condicional añadir secciones imagen - texto*/
if (isset($_GET["img"])) {
    $var = count(glob('pagina/inicio/{*.php}', GLOB_BRACE));
    copy("plantillas/ImagenTexto/imagenTexto" . $_GET["img"] . ".php", "pagina/inicio/seccion" . $var . ".php");
    eliminar("<!-- ed -->", "pagina/inicio/inicio.php", "");
    Editar("<!-- area" . $var . " -->", "pagina/inicio/inicio.php", "\t<!-- area" . $var . " -->" . PHP_EOL . "\t<!-- ed --><div class='secos'><br><h6>EDITANDO ESTA SECCION</h6></div>");
}
/* Condicional impresion de seccion */
if (isset($_GET["sec"])) {
    $edicion = Contener($_GET["sec"]);
    $car = "#seccion" . $_GET["sec"];
}

if (isset($_GET["edicion"])) {
    $edicion = $_GET["edicion"];
}
if (isset($_GET["b"])) {
?>
    <div id="vista">
        <div id="pag" class="pag">
            <iframe class="embed-responsive-item frame" src="pagina/index.php<?php echo $car ?>" width="100%" allowfullscreen></iframe>
        </div>
    </div>
<?php
} else {
?>
    <div id="pag" class="pag">
        <iframe class="embed-responsive-item frame" src="pagina/index.php<?php echo $car ?>" width="100%"></iframe>
    </div>
<?php
}
?>
<script>
    if (<?php echo $edicion ?> != "") {
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/TipoAjax.php") ?>&t=" + <?php echo $edicion; ?>;
            $("#tipo").load(url);
        })
    }
    if (<?php echo $_GET["sec"] ?> == 0) {
        $(document).ready(function() {
            var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/partes/TipoAjax.php") ?>&t=0";
            $("#tipo").load(url);
        })
    }
</script>